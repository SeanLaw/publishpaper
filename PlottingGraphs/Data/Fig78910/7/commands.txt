set xlabel "Gamma"       
set ylabel "Number of Swaps" 



set yr [100:200]
set xr [1.8:]  

set title "CvD Swaps to reach average 1.5 payoff/game"




f(x) = a*x + b    
fit f(x) "7cvd.txt" using 1:2 via a,b



plot "7cvd.txt" u 1:2 t "" w points lt 2 lc rgb "#000000", \
f(x) t "" w lines lc rgb "#000000" dt '.'   